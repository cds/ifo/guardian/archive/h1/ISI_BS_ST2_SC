from ezca.ligofilter import LIGOFilter
from .. import const as top_const
from . import const

# FIXME: This doesn't really tell whether or not a filter is switching
def is_switching_filters(SC_bank, deg_of_free):
    """
    Returns True if one of the SC filters for the managed degrees of
    freedom in a specified bank is ramping its gain, which suggests that it will
    be transitioning to a different filter.
    """
    ligofilter = LIGOFilter(const.SC_FILTER_CHANNEL_NAME.format(deg_of_free=deg_of_free,bank=SC_bank),ezca)
    if ligofilter.is_gain_ramping():
        return True
    return False

def bank_check(bank_list):
    """Checks that the list of banks is valid. Returns True if all banks are good."""
    for bank in bank_list:
        if bank not in const.SC_FILTER_BANKS:
            return False
    return True
